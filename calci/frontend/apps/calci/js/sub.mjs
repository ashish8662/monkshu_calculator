import { xhr } from "/framework/js/xhr.mjs";

const substract = async () => {
    const results = await xhr.rest(APP_CONSTANTS.API_SUB,"POST", {a:document.getElementById('a').value, b:document.getElementById('b').value});
    if (results.result) document.getElementById('out').innerHTML = results.data;
}
export const sub = { substract}