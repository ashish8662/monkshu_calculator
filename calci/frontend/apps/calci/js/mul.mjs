import { xhr } from "/framework/js/xhr.mjs";

const multiply = async () => {
    const results = await xhr.rest(APP_CONSTANTS.API_MUL,"POST", {a:document.getElementById('a').value, b:document.getElementById('b').value});
    if (results.result) document.getElementById('out').innerHTML = results.data;
}
export const mul = { multiply}