/* 
 * (C) 2015 TekMonks. All rights reserved.
 * License: GPL2 - see enclosed LICENSE file.
 */

exports.doService = async jsonReq => {
    if(!validateRequest(jsonReq)) return API_CONSTANTS.API_INSUFFICIENT_PARAMS;
    try {
       return { "result": true, "data":a(jsonReq) };
    } catch (err) { LOG.error(err); return CONSTANTS.FALSE_RESULT; }

}

const a = jsonReq => JSON.parse(jsonReq.a) - JSON.parse(jsonReq.b);

const validateRequest = jsonReq => {
    if(jsonReq && jsonReq.a && jsonReq.b) return true;
    else if(!isNaN(JSON.parse(jsonReq.a)) || !isNaN(JSON.parse(jsonReq.b))) return true;
    else return CONSTANTS.FALSE_RESULT;
}